import uuid
import json
import boto3
from boto3.dynamodb.conditions import Attr
import pandas as pd
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.models import Variable
from airflow.hooks.postgres_hook import PostgresHook

dynamodb = boto3.resource('dynamodb',
                           region_name=Variable.get("AWS_DEFAULT_REGION"),
                           aws_access_key_id=Variable.get("AWS_ACCESS_KEY_ID"),
                           aws_secret_access_key=Variable.get("AWS_SECRET_ACCESS_KEY"))

s3 = boto3.resource('s3',
                           region_name=Variable.get("AWS_DEFAULT_REGION"),
                           aws_access_key_id=Variable.get("AWS_ACCESS_KEY_ID"),
                           aws_secret_access_key=Variable.get("AWS_SECRET_ACCESS_KEY"))

s3Hook = S3Hook(aws_conn_id="aws_default")

customer_sql_path=Variable.get("CUSTOMER_SQL_PATH")
time_sql_path=Variable.get("TIME_SQL_PATH")
items_sql_path=Variable.get("ITEMS_SQL_PATH")
bought_items_sql_path=Variable.get("BOUGHT_ITEMS_SQL_PATH")


def trim_all_columns(df):
    trim_strings = lambda x: x.strip() if isinstance(x, str) else x
    return df.applymap(trim_strings)

def load_info_from_dynamodb_to_s3(table_name, file_name, date, **kwargs):
    table = dynamodb.Table(table_name)
    response = table.scan(FilterExpression=Attr('created_date').eq(date))
    s3object = s3.Object(Variable.get("S3_BUCKET_NAME"),  file_name)
    s3object.put(
        Body=(bytes(json.dumps(response['Items']).encode('utf-8')))
    )

def load_customer_data_from_s3_to_redshift(table_name, path, file_name,**kwargs):
    s3Hook = S3Hook().get_key(key=f"{path}/{file_name}", bucket_name=Variable.get("S3_BUCKET_NAME"))
    raw_csv_df = pd.read_csv(s3Hook.get()['Body'], sep=",")
    redshift_hook = PostgresHook(postgres_conn_id='redshift')
    engine = redshift_hook.get_sqlalchemy_engine()
    conn = redshift_hook.get_conn()
    customer_filtered_df = avoid_duplicates_rows(conn, "id", customer_sql_path, raw_csv_df)
    customer_filtered_df.to_sql(table_name, con=engine, schema='public', if_exists='append', index=False)

def load_item_data_from_s3_to_redshift(table_name, path, file_name,**kwargs):
    s3Hook = S3Hook().get_key(key=f"{path}/{file_name}", bucket_name=Variable.get("S3_BUCKET_NAME"))
    raw_csv_df = pd.read_csv(s3Hook.get()['Body'], sep=",")
    redshift_hook = PostgresHook(postgres_conn_id='redshift')
    engine = redshift_hook.get_sqlalchemy_engine()
    conn = redshift_hook.get_conn()
    items_filtered_df = avoid_duplicates_rows(conn, "id", items_sql_path , raw_csv_df)
    items_filtered_df.to_sql(table_name, con=engine, schema='public', if_exists='append', index=False)

def load_item_bought_data_and_time_csv_to_redshift(table_name1, table_name2, path, file_name,**kwargs):
    s3Hook = S3Hook().get_key(key=f"{path}/{file_name}", bucket_name=Variable.get("S3_BUCKET_NAME"))
    raw_csv_df = pd.read_csv(s3Hook.get()['Body'], sep=",")
    time_df = raw_csv_df.copy()
    time_df = time_df.drop(columns=['id', 'order_number', 'customer_id', 'item_id', 'comments'])
    raw_csv_df = raw_csv_df.drop(columns=['created_date','comments'])
    time_id = str(uuid.uuid1())
    time_df.loc[:, 'year'] = pd.DatetimeIndex(time_df['created_date']).year
    time_df.loc[:, 'month'] = pd.DatetimeIndex(time_df['created_date']).month
    time_df.loc[:, 'day'] = pd.DatetimeIndex(time_df['created_date']).day
    time_df = time_df.drop_duplicates(subset=['year', 'month','day'], keep='last')
    time_df.loc[:, 'id'] = time_df.apply(lambda x: time_id, axis=1)
    raw_csv_df.loc[:, 'time_id'] = time_id
    redshift_hook = PostgresHook(postgres_conn_id='redshift')
    engine = redshift_hook.get_sqlalchemy_engine()
    conn = redshift_hook.get_conn()
    time_filtered_df = avoid_duplicates_rows(conn, "created_date", time_sql_path, time_df)
    bought_items_filtered_df = avoid_duplicates_rows(conn, "id", bought_items_sql_path, raw_csv_df)
    bought_items_filtered_df = trim_all_columns(bought_items_filtered_df)
    time_filtered_df.to_sql(table_name1, con=engine, schema='public', if_exists='append', index=False)
    bought_items_filtered_df.to_sql(table_name2, con=engine, schema='public', if_exists='append', index=False)

def load_customer_data_json_to_redshift(table_name, path, file_name,**kwargs):
    s3Hook = S3Hook().get_key(key=f"{path}/{file_name}", bucket_name=Variable.get("S3_BUCKET_NAME"))
    raw_csv_json = pd.read_json(s3Hook.get()['Body'])
    redshift_hook = PostgresHook(postgres_conn_id='redshift')
    engine = redshift_hook.get_sqlalchemy_engine()
    conn = redshift_hook.get_conn()
    customer_filtered_df = avoid_duplicates_rows(conn, "id", customer_sql_path, raw_csv_json)
    customer_filtered_df.to_sql(table_name, con=engine, schema='public', if_exists='append', index=False)

def load_items_data_json_to_redshift(table_name, path, file_name,**kwargs):
    s3Hook = S3Hook().get_key(key=f"{path}/{file_name}", bucket_name=Variable.get("S3_BUCKET_NAME"))
    raw_json_df = pd.read_json(s3Hook.get()['Body'])
    raw_json_df.rename(columns={'title': 'name'}, inplace=True)
    redshift_hook = PostgresHook(postgres_conn_id='redshift')
    engine = redshift_hook.get_sqlalchemy_engine()
    conn = redshift_hook.get_conn()
    items_filtered_df = avoid_duplicates_rows(conn, "id", items_sql_path, raw_json_df)
    items_filtered_df.to_sql(table_name, con=engine, schema='public', if_exists='append', index=False)

def load_item_bought_data_and_time_json_to_redshift(table_name, path, file_name,**kwargs):
    s3Hook = S3Hook().get_key(key=f"{path}/{file_name}", bucket_name=Variable.get("S3_BUCKET_NAME"))
    raw_json_df = pd.read_json(s3Hook.get()['Body'])
    time_df = raw_json_df.copy()
    time_df = time_df.drop(columns=['id', 'customer', 'item'])
    redshift_hook = PostgresHook(postgres_conn_id='redshift')
    #engine = redshift_hook.get_sqlalchemy_engine()
    conn = redshift_hook.get_conn()
    time_filtered_df = avoid_duplicates_rows(conn, "created_date", time_sql_path, time_df)

def avoid_duplicates_rows(conn, column, sql_path, df):
    query_filter = get_query_filter(df,column)
    read_df = pd.read_sql(open(sql_path).read(), conn, params=[query_filter])
    filtered_df = df.merge(read_df, how='left', on=(column))
    filtered_df = filtered_df[filtered_df['exists'] != 'true']
    filtered_df = filtered_df.drop('exists', 1)
    return filtered_df

def get_query_filter(df, column):
    return tuple((aux_df[column])
                 for index, aux_df in df.iterrows())