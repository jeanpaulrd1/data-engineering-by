from datetime import datetime
from airflow.decorators import dag
from airflow.providers.amazon.aws.transfers.mysql_to_s3 import MySQLToS3Operator
from airflow.operators.python_operator import PythonOperator
from airflow.utils.task_group import TaskGroup
from airflow.operators.dummy import DummyOperator
from dags.aws_pipeline.scripts.functions import (load_info_from_dynamodb_to_s3,
                                                 load_customer_data_from_s3_to_redshift,
                                                 load_item_data_from_s3_to_redshift,
                                                 load_item_bought_data_and_time_csv_to_redshift,
                                                 load_customer_data_json_to_redshift,
                                                 load_items_data_json_to_redshift,
                                                 load_item_bought_data_and_time_json_to_redshift)

@dag(

    dag_id="bankaya_boughts_dag",
    catchup=False,
    schedule_interval='10 0 * * *',
    default_args={
        'start_date': datetime(2022, 1, 21),
        'owner': 'airflow'
    }
)

def load_items_bought_data():
    start = DummyOperator(task_id='start')
    end = DummyOperator(task_id='end')

    with TaskGroup("load_data_raw", tooltip="Tasks to load data from sources to raw layer") as load_data_raw:
        task_1 = PythonOperator(
                 task_id='customer_data_from_dynamodb_to_s3',
                 provide_context=True,
                 python_callable= load_info_from_dynamodb_to_s3,
                 op_kwargs={"table_name": 'customers_data',
                   "file_name":'raw_data/source1/'+ "{{ ts_nodash }}" + '/customer_data.json',
                   "date" : "{{ ts_nodash }}"}
        )
        task_2 = PythonOperator(
        task_id='items_data_from_dynamodb_to_s3',
        provide_context=True,
        python_callable=load_info_from_dynamodb_to_s3,
        op_kwargs={"table_name": 'items_data',
                   "file_name": 'raw_data/source1/' + "{{ ts_nodash }}" + '/items_data.json',
                   "date": "{{ ts_nodash }}"}
    )
        task_3 = PythonOperator(
        task_id='items_bought_data_from_dynamodb_to_s3',
        provide_context=True,
        python_callable=load_info_from_dynamodb_to_s3,
        op_kwargs={"table_name": 'items_bought_data',
                   "file_name": 'raw_data/source1/' + "{{ ts_nodash }}" + '/bought_items_data.json',
                   "date": "{{ ts_nodash }}"}
    )

        task_4 = MySQLToS3Operator(
            task_id="customer_data_from_rds_to_s3",
            query="SELECT id, first_name, last_names, phone_number, curp, rfc, "
                  "CONVERT(created_date,DATE) as created_date FROM customer where "
                  "CONVERT(created_date,DATE)=" + "'{{ ts_nodash }}'",
            s3_bucket="boughts-dwh-by",
            s3_key="raw_data/source2/" + "{{ ts_nodash }}" + "/customer_data.csv",
            mysql_conn_id="mysql_default",
            aws_conn_id="aws_default",
            header=True
        )
        task_5 = MySQLToS3Operator(
            task_id="items_data_from_rds_to_s3",
            query="SELECT id, name, price, CONVERT(created_date,DATE) as "
                  "created_date FROM items where CONVERT(created_date,DATE)= "
                  + "'{{ ts_nodash }}'",
            s3_bucket="boughts-dwh-by",
            s3_key="raw_data/source2/" + "{{ ts_nodash }}" + "/items_data.csv",
            mysql_conn_id="mysql_default",
            aws_conn_id="aws_default",
            header=True
        )
        task_6 = MySQLToS3Operator(
            task_id="items_bought_data_from_rds_to_s3",
            query="SELECT id, order_number, customer_id, item_id, "
                  "created_date, comments "
                  "FROM items_bought where CONVERT(created_date,DATE)= "
                  + "'{{ ts_nodash }}'",
            s3_bucket="boughts-dwh-by",
            s3_key="raw_data/source2/" + "{{ ts_nodash }}" + "/items_bought_data.csv",
            mysql_conn_id="mysql_default",
            aws_conn_id="aws_default",
            header=True
        )


    with TaskGroup("load_data_master", tooltip="Tasks to load data from raw layer to master") as load_data_master:
        task_7 = PythonOperator(
        task_id='customer_data_json_from_s3_to_redshift',
        provide_context=True,
        python_callable=load_customer_data_json_to_redshift,
        op_kwargs={"table_name":"customer",
                   "path": 'raw_data/source1/' + "{{ ts_nodash }}",
                   "file_name": 'customer_data.json'}
    )
        task_8 = PythonOperator(
        task_id='items_data_json_from_s3_to_redshift',
        provide_context=True,
        python_callable=load_items_data_json_to_redshift,
        op_kwargs={"table_name": "items",
                   "path": 'raw_data/source1/' + "{{ ts_nodash }}",
                   "file_name": 'items_data.json'}
    )
        task_9 = PythonOperator(
        task_id='items_bought_data_json_from_s3_to_redshift',
        provide_context=True,
        python_callable=load_item_bought_data_and_time_json_to_redshift,
        op_kwargs={"table_name": "",
                   "path": 'raw_data/source1/' + "{{ ts_nodash }}",
                   "file_name": 'bought_items_data.json'}
    )

        task_10 = PythonOperator(
        task_id = "customer_data_from_s3_to_redshift",
        provide_context=True,
        python_callable=load_customer_data_from_s3_to_redshift,
        op_kwargs={"table_name":"customer",
                   "path": 'raw_data/source2/' + "{{ ts_nodash }}",
                   "file_name": 'customer_data.csv'},
    )
        task_11 = PythonOperator(
        task_id="items_data_from_s3_to_redshift",
        provide_context=True,
        python_callable=load_item_data_from_s3_to_redshift,
        op_kwargs={"table_name": "items",
                   "path": 'raw_data/source2/' + "{{ ts_nodash }}",
                   "file_name": 'items_data.csv'},
    )
        task_12 = PythonOperator(
        task_id="time_item_bought_data_csv_from_s3_to_redshift",
        provide_context=True,
        python_callable=load_item_bought_data_and_time_csv_to_redshift,
        op_kwargs={"table_name1": "time",
                   "table_name2": "boughts_facts",
                   "path": 'raw_data/source2/' + "{{ ts_nodash }}",
                   "file_name": 'items_bought_data.csv'},
    )

    start >> load_data_raw >> load_data_master >> end

dag = load_items_bought_data()